========
fbTrends
========

fbTrends is a project based on Facebook. The purpose of the project is to study and analyze different trends in facebook. The data required for the study is obtained from a large user base. 
The project uses fbconsole to log into facebook. Then, it collects data since the conception of the user's facebook account till date and stores it in a central database. This data, collected over a period of time for multiple users is then studied extensively and usage trends in facebook are analyzed and used for research purposes. The data collected here shall be used strictly for above-mentioned purposes, including research and development.

Special thanks to Apoorv Ashutosh for his invaluable help in the course of the project.

Dependencies :
--------------

1. fbconsole
2. requests
3. signal
4. sys
5. calendar
6. sqlite3 
7. datetime

Install the Dependencies:
--------------------------------

Dependencies *signal*, *sys*, *calendar*, *datetime*, *sqlite3* comes preinstalled with python.

1. fbconsole    : http://github.com/facebook/fbconsole

You might need to upgrade requests. Execute the following code to upgrade requests::
    
    pip install requests --upgrade





